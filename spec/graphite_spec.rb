RSpec.describe DateParser::Graphite do

  it "Parse relative seconds correctly" do
    now = DateTime.now
    expect(now - DateParser::Graphite.parse("-3600s", now)).to eq(Rational(1,24))
  end

  it "Parse relative minutes correctly" do
    now = DateTime.now
    expect(now - DateParser::Graphite.parse("-120m", now)).to eq(Rational(1,12))
  end

  it "Parse relative hours correctly" do
    now = DateTime.now
    expect(now - DateParser::Graphite.parse("-1h", now)).to eq(Rational(1,24))
    expect(now - DateParser::Graphite.parse("-24h", now)).to eq(Rational(1))
  end

  it "Parse relative days correctly" do
    now = DateTime.now
    expect(now - DateParser::Graphite.parse("-1d", now)).to eq(1)
  end

  it "Parse relative weeks correctly" do
    now = DateTime.now
    expect(now - DateParser::Graphite.parse("-1w", now)).to eq(7)
  end

  it "Parse relative months correctly" do
    now = DateTime.now
    expect(now - DateParser::Graphite.parse("-1mon", now)).to eq(30)
    expect(now - DateParser::Graphite.parse("-4mon", now)).to eq(120)
  end

  it "Parse relative years correctly" do
    now = DateTime.now
    expect(now - DateParser::Graphite.parse("-1y", now)).to eq(365)
    expect(now - DateParser::Graphite.parse("-2y", now)).to eq(730)
  end
end
