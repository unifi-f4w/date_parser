require "date"

module DateParser
  # #graphite_time_parser
  # Parse a time string as specified by the from/until
  # format http://graphite.readthedocs.io/en/latest/render_api.html#from-until
  class Graphite
    def self.parse(timestring, now=DateTime.now)
      if timestring.to_s.empty?
        raise ArgumentError, "timestring empty"
      end
      if timestring[0] == "-"
        # is a relative time string
        # there should be at least one digit and one alphabetic character
        digits, unit = timestring.scan(/([[:digit:]]+)([[:alpha:]]+)/).flatten
        # returns the number of days
        # described by the unit
        days = case unit
        when "s"
          Rational(1, 24 * 60 * 60)
        when "m"
          Rational(1, 24 * 60)
        when "h"
          Rational(1,  24)
        when "d"
          1
        when "w"
          7
        when "mon"
          30
        when "y"
          365
        end
        now - (digits.to_i * days)
      else
        # let ruby parse it
        DateTime.parse(timestring)
      end
    end
  end
end
